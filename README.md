### What is this repository for? ###
On Android all alarms will disappear after device reboot. This repo contains a library that allows to set, cancel and persist alarms. 

### How do I get set up? ###

* Clone/download repo
* Build it with:
        ./gradlew :immortalalarms:build
* Grab immortalalarms-release.aar, put it in your libs folder
* Add the following to your build gradle:

```
#!groovy
repositories{
    flatDir{
        dirs 'libs'
    }
}

dependencies {
    compile(name:'immortalalarms-release', ext:'aar')
}
```

Create and schedule alarms:

       Intent intent = new Intent(this, MyReceiver.class);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 5);
        Alarm alarm = Alarm.builder()
                .intent(intent)
                .time(calendar.getTimeInMillis())
                .type(AlarmManager.RTC)
                .schedulingType(Alarm.Type.REPEATING)
                .requestCode(1)
                .flags(PendingIntent.FLAG_UPDATE_CURRENT)
                .build();
        AlarmScheduler scheduler = AlarmScheduler.getInstance(this);
        scheduler.schedule(alarm);

Save alarms to a file:

       Scheduler.saveAlarms();

Cancel alarms:

       Scheduler.cancleAlarms();
       Scheduler.canclerAlarm(alarm);

After reboot alarms will be set automatically.

You need to save your alarms after each call that sets/cancles alarms.
getInstance and saveAlarms perform disk operation so they should be called off main thread;