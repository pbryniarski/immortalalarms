package com.bryn.immortalalarms;

import android.content.Intent;

import com.google.auto.value.AutoValue;

/**
 * Created by pawelbryniarski on 17.04.16.
 */
@AutoValue
public abstract class Alarm {

    public static Builder builder() {
        return new AutoValue_Alarm.Builder()
                .interval(-1); // the rest is required
    }

    public abstract long time();

    public abstract int type();

    public abstract Intent intent();

    public abstract Type schedulingType();

    public abstract int requestCode();

    public abstract int flags();

    public abstract long interval();

    public enum Type {
        EXACT,
        INEXACT,
        REPEATING
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder time(long value);

        public abstract Builder intent(Intent value);

        public abstract Builder type(int value);

        public abstract Builder requestCode(int value);

        public abstract Builder flags(int value);

        public abstract Builder schedulingType(Type value);

        public abstract Builder interval(long value);

        public abstract Alarm build();
    }
}
