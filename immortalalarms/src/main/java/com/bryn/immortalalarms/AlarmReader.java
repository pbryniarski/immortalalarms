package com.bryn.immortalalarms;

import android.content.Intent;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by pawelbryniarski on 17.04.16.
 */
class AlarmReader {

    private final FileReader fileReader;

    AlarmReader(FileReader fileReader) {
        this.fileReader = fileReader;
    }

    Collection<Alarm> readAlarms() throws URISyntaxException {
        if (!fileReader.open()) {
            return Collections.emptyList();
        }
        List<Alarm> alarmList = new ArrayList<>();
        String line;
        while ((line = fileReader.readLine()) != null) {
            alarmList.add(
                    Alarm.builder()
                            .time(Long.valueOf(line))
                            .type(Integer.valueOf(fileReader.readLine()))
                            .intent(Intent.parseUri(fileReader.readLine(), Intent.URI_INTENT_SCHEME))
                            .schedulingType(Alarm.Type.valueOf(fileReader.readLine()))
                            .requestCode(Integer.valueOf(fileReader.readLine()))
                            .flags(Integer.valueOf(fileReader.readLine()))
                            .interval(Integer.valueOf(fileReader.readLine()))
                            .build());
        }
        return alarmList;
    }

}
