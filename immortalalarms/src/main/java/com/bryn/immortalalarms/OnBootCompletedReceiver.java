package com.bryn.immortalalarms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnBootCompletedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        DebugLogger.log("OnBootCompletedReceiver", "Rescheduling alarms");
        AlarmScheduler alarmScheduler = AlarmScheduler.getInstance(context);
        alarmScheduler.rescheduleAlarms();
    }
}
