package com.bryn.immortalalarms;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by pawelbryniarski on 17.04.16.
 */
class FileWriter {
    private final String path;
    private PrintWriter printWriter;

    FileWriter(String path) {
        this.path = path;
    }

    boolean open() {
        try {
            printWriter = new PrintWriter(new java.io.FileWriter(path));
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    void delete() {
        new File(path).delete();
    }

    void printLine(String line) {
        printWriter.println(line);
    }

    void printLine(long line) {
        printWriter.println(line);
    }

    void printLine(boolean line) {
        printWriter.println(line);
    }

    void close() {
        if (printWriter != null) {
            printWriter.close();
        }
    }
}
