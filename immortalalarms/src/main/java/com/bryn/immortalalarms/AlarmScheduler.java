package com.bryn.immortalalarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;

import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

public class AlarmScheduler {

    private static final String TAG = "AlarmScheduler";
    private static AlarmScheduler instance;
    private final Context context;
    private final AlarmManager alarmManager;
    private final AlarmSaver alarmSaver;
    private final AlarmReader alarmReader;
    private Set<Alarm> alarms;

    private AlarmScheduler(Context context) {
        this.context = context;
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        String path = context.getFilesDir() + "/persist_alarms";
        alarmSaver = new AlarmSaver(new FileWriter(path));
        alarms = new HashSet<>();
        alarmReader = new AlarmReader(new FileReader(path));
        try {
            DebugLogger.log(TAG, "Reading alarms");
            readAlarms();
        } catch (URISyntaxException e) {
            DebugLogger.log(TAG, "Error: ", e);
        }
    }

    @VisibleForTesting
    AlarmScheduler(Context context, AlarmManager alarmManager, AlarmSaver alarmSaver, AlarmReader alarmReader) {
        this.context = context;
        this.alarmManager = alarmManager;
        this.alarmSaver = alarmSaver;
        this.alarmReader = alarmReader;
    }

    @WorkerThread
    synchronized public static AlarmScheduler getInstance(Context context) {
        if (instance == null) {
            instance = new AlarmScheduler(context);
        }
        return instance;
    }

    private void readAlarms() throws URISyntaxException {
        alarms.addAll(alarmReader.readAlarms());
    }

    public boolean schedule(Alarm alarm) {
        PendingIntent intent = PendingIntent.getBroadcast(context, alarm.requestCode(), alarm.intent(), alarm.flags());
        switch (alarm.schedulingType()) {
            case EXACT:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    alarmManager.set(alarm.type(), alarm.time(), intent);
                } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    alarmManager.setExact(alarm.type(), alarm.time(), intent);
                } else {
                    alarmManager.setExactAndAllowWhileIdle(alarm.type(), alarm.time(), intent);
                }
                break;
            case INEXACT:
                alarmManager.set(alarm.type(), alarm.time(), intent);
                break;
            case REPEATING:
                alarmManager.setRepeating(alarm.type(), alarm.time(), alarm.interval(), intent);
                break;
            default:
                return false;
        }
        alarms.add(alarm);
        return true;
    }

    @WorkerThread
    public void saveAlarms() {
        alarmSaver.saveAlarms(alarms);
    }

    public void cancelAlarm(Alarm alarm) {
        Set<Alarm> tempAlarms = new HashSet<>(alarms);
        tempAlarms.remove(alarm);
        cancelAlarms();
        alarms = tempAlarms;
    }

    public boolean isAlarmSet(Alarm alarm) {
        return alarms.contains(alarm);
    }

    public void cancelAlarms() {
        for (Alarm alarm : alarms) {
            PendingIntent intent = PendingIntent.getBroadcast(context, alarm.requestCode(), alarm.intent(), alarm.flags());
            alarmManager.cancel(intent);
        }
        alarms.clear();
    }

    void rescheduleAlarms() {
        for (Alarm alarm : alarms) {
            schedule(alarm);
        }
    }
}
