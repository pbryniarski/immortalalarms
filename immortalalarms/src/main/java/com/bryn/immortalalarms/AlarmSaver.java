package com.bryn.immortalalarms;

import android.content.Intent;

import java.util.Collection;

/**
 * Created by pawelbryniarski on 17.04.16.
 */
class AlarmSaver {

    private final FileWriter writer;

    AlarmSaver(FileWriter writer) {
        this.writer = writer;
    }

    void saveAlarms(Collection<Alarm> alarms) {
        writer.delete();
        if (!writer.open()) {
            return;
        }
        for (Alarm alarm : alarms) {
            writeAlarm(alarm);
        }
        writer.close();
    }

    private void writeAlarm(Alarm alarm) {
        writer.printLine(alarm.time());
        writer.printLine(alarm.type());
        writer.printLine(alarm.intent().toUri(Intent.URI_INTENT_SCHEME));
        writer.printLine(alarm.schedulingType().name());
        writer.printLine(alarm.requestCode());
        writer.printLine(alarm.flags());
        writer.printLine(alarm.interval());
    }

    void clear() {
        writer.delete();
    }
}