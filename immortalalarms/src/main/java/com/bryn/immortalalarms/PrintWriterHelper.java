package com.bryn.immortalalarms;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by pawelbryniarski on 17.04.16.
 */
class PrintWriterHelper {

    PrintWriter getPrintWriter(String path) {
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(new java.io.FileWriter(path));
        } catch (IOException e) {
            return null;
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
        return printWriter;
    }

    void closePrintWriter(PrintWriter printWriter) {
        if (printWriter != null) {
            printWriter.close();
        }
    }
}
