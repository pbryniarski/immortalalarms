package com.bryn.immortalalarms;

import android.util.Log;

class DebugLogger {
    static void log(final String tag, final String message) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, message);
        }
    }

    static void log(final String tag, final String message, Exception e) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, message, e);
        }
    }
}
