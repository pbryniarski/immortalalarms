package com.bryn.immortalalarms;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by pawelbryniarski on 17.04.16.
 */
class FileReader {

    private final String path;
    private BufferedReader br;

    FileReader(String path) {
        this.path = path;
        Log.e("READ", "path " + path);
    }

    boolean open() {
        try {
            br = new BufferedReader(new java.io.FileReader(path));
        } catch (IOException e) {
            Log.e("READ", "error opening file");
            return false;
        }
        return true;
    }

    String readLine() {
        try {
            return br.readLine();
        } catch (IOException e) {
            Log.e("READ", "error reading line in file");
            return null;
        }
    }

    void close() {
        if (br != null) {
            try {
                br.close();
            } catch (IOException e) {
                //Ignore
            }
        }
    }
}
