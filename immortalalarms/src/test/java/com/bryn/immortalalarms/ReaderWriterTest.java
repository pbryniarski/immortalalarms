package com.bryn.immortalalarms;

import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static junit.framework.Assert.assertEquals;


public class ReaderWriterTest {

    private String path;
    private FileReader fileReader;
    private FileWriter fileWriter;
    private Alarm alarm;

    @Before
    public void setUp() throws Exception {
        String path = File.createTempFile("temp", "file").getAbsolutePath();
        fileReader = new FileReader(path);
        fileWriter = new FileWriter(path);
//        Alarm alarm = Alarm.builder().build();
    }

    @Test
    public void testWriteRead() throws Exception {
        fileWriter.open();
        fileWriter.printLine("ABC");
        fileWriter.printLine(true);
        fileWriter.printLine(1234);
        fileWriter.close();

        fileReader.open();
        assertEquals("ABC", fileReader.readLine());
        assertEquals(Boolean.TRUE, Boolean.valueOf(fileReader.readLine()));
        assertEquals(Integer.valueOf(1234),Integer.valueOf(fileReader.readLine()));
    }
}
