package com.bryn.persistentalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bryn.immortalalarms.Alarm;
import com.bryn.immortalalarms.AlarmScheduler;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AlarmScheduler scheduler = AlarmScheduler.getInstance(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intent intent = new Intent(this, MyReceiver.class);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 5);
        Alarm alarm = Alarm.builder()
                .intent(intent)
                .time(calendar.getTimeInMillis())
                .type(AlarmManager.RTC)
                .schedulingType(Alarm.Type.REPEATING)
                .requestCode(1)
                .flags(PendingIntent.FLAG_UPDATE_CURRENT)
                .build();
        AlarmScheduler scheduler = AlarmScheduler.getInstance(this);
        scheduler.schedule(alarm);
        scheduler.saveAlarms();
    }
}
